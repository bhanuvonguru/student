import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Student {

    private String name;
    private int id;

    Student(String name,int id){
        this.name = name;
        this.id =id;
    }

    Student(){
    }

    List<String> performStreamOperations(List<Student> students){
        if(students== null) return new ArrayList<>();
        List<String> listOfStudents=students.stream()
                .map(s -> s.name).collect(Collectors.toList());
        System.out.println("list of names"+listOfStudents);
        return listOfStudents;
    }
}
