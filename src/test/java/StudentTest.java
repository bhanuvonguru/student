import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StudentTest {
    List<Student> students = new ArrayList<>();
    Student student = new Student();
    @BeforeEach
    void setUp(){
        students.add(new Student("bhanu", 1));
        students.add(new Student("Shruti", 2));
        students.add(new Student("Anita", 3));
        students.add(new Student("Raghave", 4));
    }
    @Test
    void ShouldReturnNullIfListOfNamesAreNull(){
        assertEquals(new ArrayList<String>(),student.performStreamOperations(null));
    }

    @Test
    void ShouldReturnListOfNames(){
        assertEquals(new ArrayList<String>(
                Arrays.asList("bhanu","Shruti","Anita","Raghave")),student.performStreamOperations(students));
    }
}
